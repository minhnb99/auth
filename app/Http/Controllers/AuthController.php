<?php

namespace App\Http\Controllers;

use App\Actions\GenerateSessionKey;
use App\Actions\CheckCredential;
use App\Actions\SetDataSource;
use App\Http\Requests\LoginRequest;
use App\Services\SessionKeyStorage\DatabaseSessionKeyStorage;
use App\Services\SessionKeyStorage\RedisSessionKeyStorage;
use App\Services\SessionKeyStorage\SessionSessionKeyStorage;
use Exception;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    /**
     * @param LoginRequest $request
     * @param SetDataSource $setDataSource
     * @param CheckCredential $handleLogin
     * @param DatabaseSessionKeyStorage $databaseSessionKeyStorage
     * @param RedisSessionKeyStorage $redisSessionKeyStorage
     * @param SessionSessionKeyStorage $sessionSessionKeyStorage
     * @param GenerateSessionKey $generateSessionKey
     * @return JsonResponse
     * @throws Exception
     */
    public function login(
        LoginRequest              $request,
        SetDataSource             $setDataSource,
        CheckCredential           $handleLogin,
        DatabaseSessionKeyStorage $databaseSessionKeyStorage,
        RedisSessionKeyStorage    $redisSessionKeyStorage,
        SessionSessionKeyStorage  $sessionSessionKeyStorage,
        GenerateSessionKey        $generateSessionKey
    ): JsonResponse
    {
        $credentials = $request->validated();

        $setDataSource($credentials['data_source']);
        $user = $handleLogin($credentials);
        $sessionKey = $generateSessionKey($user->email);

        $databaseSessionKeyStorage->handle($sessionKey);
        $redisSessionKeyStorage->handle($sessionKey);
        $sessionSessionKeyStorage->handle($sessionKey);

        return response()->json([
            'status' => true,
            'message' => 'Login successful',
            'data'  => $sessionKey
        ]);
    }

    public function logout()
    {
        //
    }

}
