<?php

namespace App\Services\SessionKeyStorage;

use App\Contracts\StoreSessionKeyInterface;
use App\Models\SessionKey;

class DatabaseSessionKeyStorage implements StoreSessionKeyInterface
{
    public function handle($key): void
    {
        SessionKey::create(['key' => $key]);
    }
}
