<?php

namespace App\Services\SessionKeyStorage;

use App\Contracts\StoreSessionKeyInterface;
use Illuminate\Support\Facades\Session;

class SessionSessionKeyStorage implements StoreSessionKeyInterface
{
    public function handle($key): void
    {
        Session::put('session_key', $key);
    }
}
