<?php

namespace App\Services\SessionKeyStorage;

use App\Contracts\StoreSessionKeyInterface;
use Illuminate\Support\Facades\Redis;

class RedisSessionKeyStorage implements StoreSessionKeyInterface
{
    public function handle($key): void
    {
        Redis::set('session_key', $key);
    }
}
