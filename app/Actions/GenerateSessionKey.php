<?php

namespace App\Actions;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class GenerateSessionKey
{
    /**
     * @param string $email
     * @return string
     */
    public function __invoke(string $email): string
    {
        return Hash::make($email);
    }
}
