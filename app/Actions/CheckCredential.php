<?php

namespace App\Actions;

use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use function PHPUnit\Framework\throwException;

class CheckCredential
{
    /**
     * @param array $credentials
     * @return mixed
     * @throws Exception
     */
    public function __invoke(array $credentials): mixed
    {
        $user = User::where('email', $credentials['email'])->first();

        if (!$user || !Hash::check($credentials['password'], $user->password))
        {
            throw new Exception('Invalid credentials');
        }

        return $user;
    }
}
