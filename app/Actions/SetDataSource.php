<?php

namespace App\Actions;

class SetDataSource
{
    /**
     * @param string $dataSource
     * @return void
     */
    public function __invoke(string $dataSource): void
    {
        config(['database.default' => $dataSource]);
    }
}
