<?php

namespace App\Providers;

use App\Services\SessionKeyStorage\DatabaseSessionKeyStorage;
use App\Services\SessionKeyStorage\RedisSessionKeyStorage;
use App\Services\SessionKeyStorage\SessionSessionKeyStorage;
use App\Contracts\StoreSessionKeyInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(StoreSessionKeyInterface::class, RedisSessionKeyStorage::class);
        $this->app->bind(StoreSessionKeyInterface::class, DatabaseSessionKeyStorage::class);
        $this->app->bind(StoreSessionKeyInterface::class, SessionSessionKeyStorage::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
