<?php

namespace App\Contracts;

interface StoreSessionKeyInterface
{
    public function handle($key);
}
