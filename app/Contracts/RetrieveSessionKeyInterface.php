<?php

namespace App\Contracts;

interface RetrieveSessionKeyInterface
{
    public function handle();
}
