<?php

namespace Tests\Feature;

use App\Actions\GenerateSessionKey;
use App\Contracts\SessionKeyStorageInterface;
use App\Models\User;
use App\Services\SessionKeyStorage\DatabaseSessionKeyStorage;
use App\Services\SessionKeyStorage\RedisSessionKeyStorage;
use App\Services\SessionKeyStorage\SessionSessionKeyStorage;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\AuthController;
use App\Actions\SetDataSource;
use App\Actions\CheckCredential;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;
use Mockery;

class AuthControllerTest extends TestCase
{
    /**
     * @return void
     * @throws Exception
     */
    public function testLogin()
    {
        $request = Mockery::mock(LoginRequest::class)->makePartial();
        $setDataSource = Mockery::mock(SetDataSource::class);
        $checkCredential = Mockery::mock(CheckCredential::class);
        $databaseSessionKeyStorage = Mockery::mock(DatabaseSessionKeyStorage::class);
        $redisSessionKeyStorage = Mockery::mock(RedisSessionKeyStorage::class);
        $sessionSessionKeyStorage = Mockery::mock(SessionSessionKeyStorage::class);
        $generateSessionKey = Mockery::mock(GenerateSessionKey::class);

        $credentials = [
            'email' => 'test@example.com',
            'password' => 'password',
            'data_source' => 'mysql',
        ];
        $user = User::factory()->make(['email' => $credentials['email'], 'password' => Hash::make($credentials['password'])]);
        $request->shouldReceive('validated')->once()->andReturn($credentials);
        $setDataSource->shouldReceive('__invoke')->once()->with($credentials['data_source']);
        $checkCredential->shouldReceive('__invoke')->once()->with($credentials)->andReturn($user);
        $generateSessionKey->shouldReceive('__invoke')->once()->with($user->email)->andReturn('session_key');
        $databaseSessionKeyStorage->shouldReceive('handle')->once()->with('session_key');
        $redisSessionKeyStorage->shouldReceive('handle')->once()->with('session_key');
        $sessionSessionKeyStorage->shouldReceive('handle')->once()->with('session_key');

        $controller = new AuthController($setDataSource, $checkCredential, $databaseSessionKeyStorage, $redisSessionKeyStorage, $sessionSessionKeyStorage, $generateSessionKey);

        $response = $controller->login($request, $setDataSource, $checkCredential, $databaseSessionKeyStorage, $redisSessionKeyStorage, $sessionSessionKeyStorage, $generateSessionKey);

        $testRes = new TestResponse($response);

        $this->assertEquals(200, $response->status());

        $testRes->assertJson([
            'status' => true,
            'message' => 'Login successful',
            'data' => 'session_key',
        ]);
    }

}
